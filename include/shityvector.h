
#pragma once

#include <stdexcept>
#include <ostream>

const int DEFAULT_SIZE = 20;

class ShityVector{
    int *vector;
    int size;

public:
    ShityVector();

    ShityVector(int size);

    ShityVector(int size, int initialNumber);

    ShityVector(const ShityVector& other);

    ShityVector(ShityVector&& other);

    int getSize() const;

    int &operator[] (int i);

    void newSize(int size);

   ShityVector& operator= (const ShityVector &other);

   ShityVector& operator= (ShityVector &&other);

    bool operator==(const ShityVector &other) const;

    bool operator!=(const ShityVector &other) const;

    bool operator<(const ShityVector &other) const;

    bool operator>(const ShityVector &other) const;

    bool operator<=(const ShityVector &other) const;

    bool operator>=(const ShityVector &other) const;

    void operator+(const ShityVector &other);

    friend std::ostream &operator<<(std::ostream &os, const ShityVector &shityVector);

    friend std::istream &operator>>(std::istream &os, const ShityVector &shityVector);

    ~ShityVector();
};
