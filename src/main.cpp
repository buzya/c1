#include <iostream>
#include <cassert>
#include "../include/shityvector.h"
#include <cassert>

void testShityVector(){
    ShityVector shityVector1;
    ShityVector shityVector2(30);
    ShityVector shityVector3(30, 5);
    ShityVector shityVector4(shityVector3);
    try {
        ShityVector shityVector(-3);
    } catch(std::bad_array_new_length){
        std::cout << "Bad length" << std::endl;
    }
}

void testGetElem(){
    ShityVector shityVector(10, 4);
    assert(shityVector[5] == 4);
    try {
        shityVector[70];
    } catch(std::logic_error){
        std::cout << "Wrong Index" << std::endl;
    }
}

void testNewSize(){
    ShityVector shityVector(3, 4);
    shityVector.newSize(5);
    std::cout << shityVector;
}

void testCopy(){
    ShityVector shityVector1(3, 4);
    ShityVector shityVector2(6, 6);
    std::cout << shityVector1 << std::endl;
    std::cout << shityVector2 << std::endl;
    shityVector1 = shityVector2;
    shityVector1[3] = 2;
    std::cout << shityVector1 << std::endl;
    std::cout << shityVector2 << std::endl;
}

void testEquality(){
    ShityVector shityVector1(3, 4);
    ShityVector shityVector2(4, 5);
    try {
        bool e = shityVector1 == shityVector2;
    } catch (std::logic_error){
        std::cout << "Lengths are not the same" << std::endl;
    }
    shityVector1 = shityVector2;
    bool e = shityVector1 == shityVector2;
    std::cout << e << std::endl;
}

void testConcatination(){
    ShityVector shityVector1(3, 5);
    ShityVector shityVector2(6, 9);
    shityVector1 + shityVector2;
    std::cout << shityVector1 << std::endl;
}
void testComparicon(){
    ShityVector shityVector1(5, 0);
    ShityVector shityVector2(4, 1);
    std::cout << (shityVector2 < shityVector1) << std::endl;
    std::cout << (shityVector2 > shityVector1) << std::endl;
    shityVector1 = shityVector2;
    std::cout << (shityVector2 <= shityVector1) << std::endl;
    std::cout << (shityVector2 >= shityVector1) << std::endl;
}

int main() {
    //testShityVector();
    //testGetElem();
    //testNewSize();
    //testCopy();
    //testEquality();
    //testConcatination();
    //testComparicon();
    return 0;
}
