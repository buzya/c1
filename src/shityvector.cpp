
#include "../include/shityvector.h"

ShityVector::ShityVector() {
    this->vector = new int[DEFAULT_SIZE];
    this->size = DEFAULT_SIZE;
    for(int i = 0; i < size; i++){
        vector[i] = 0;
    }
}

ShityVector::ShityVector(int size) {
    this->vector = new int[size];
    this->size = size;
    for(int i = 0; i < size; i++){
        vector[i] = 0;
    }
}

ShityVector::ShityVector(int size, int initialNumber) {
    this->vector = new int[size];
    this->size = size;
    for(int i = 0; i < size; i++){
        vector[i] = initialNumber;
    }
}

ShityVector::ShityVector(const ShityVector &other) {
    this->size = other.size;
    this->vector = new int[this->size];
    for(int i = 0; i < this->size; i++){
        this->vector[i] = other.vector[i];
    }
}

ShityVector::~ShityVector() {
    delete[] vector;
}

int ShityVector::getSize() const {
    return size;
}

int &ShityVector::operator[](int i) {
    if(i >= size || i < 0){
        throw std::logic_error("Wrong index");
    }
    return vector[i];
}

void ShityVector::newSize(int size) {
    int *temp = new int [size];
    if(size > this->size){
        for(int i = 0; i < this->size; i++){
            temp[i] = vector[i];
        }
        for(int i = this->size; i < size; i++){
            temp[i] = 0;
        }
    } else if(size < this->size){
        for(int i = 0; i < size; i++){
            temp[i] = vector[i];
        }
    } else{
        return;
    }
    this->size = size;
    delete [] vector;
    vector = temp;
}

ShityVector &ShityVector::operator=(const ShityVector &other) {
    if (&other == this)
        return *this;
    this->size = other.size;
    newSize(this->size);
    for(int i = 0; i < this->size; i++){
        vector[i] = other.vector[i];
    }
    return *this;
}

bool ShityVector::operator==(const ShityVector &other) const {
    if(other.size != this->size){
        throw std::logic_error("Different sizes");
    }
    for(int i = 0; i < this->size; i++){
        if(this->vector[i] != other.vector[i]){
            return false;
        }
    }
    return true;
}

bool ShityVector::operator!=(const ShityVector &other) const {
    return !(other == *this);
}

bool ShityVector::operator<(const ShityVector &other) const {
    if(this->size < other.size){
        return true;
    } else if(this->size > other.size){
        return false;
    }
    for(int i = 0; i < this->size; i++){
        if(this->vector[i] >= other.vector[i]){
            return false;
        }
    }
    return true;
}

bool ShityVector::operator>(const ShityVector &other) const {
    return !(*this < other);
}

bool ShityVector::operator<=(const ShityVector &other) const {
    if(this->size < other.size){
        return true;
    } else if(this->size > other.size){
        return false;
    }
    for(int i = 0; i < this->size; i++){
        if(this->vector[i] > other.vector[i]){
            return false;
        }
    }
    return true;
}

bool ShityVector::operator>=(const ShityVector &other) const {
    if(this->size > other.size){
        return true;
    } else if(this->size < other.size){
        return false;
    }
    for(int i = 0; i < this->size; i++){
        if(this->vector[i] < other.vector[i]){
            return false;
        }
    }
    return true;
}

void ShityVector::operator+(const ShityVector &other) {
    int temp = this->size;
    newSize(this->size + other.size);
    for(int i = temp; i < this->size; i++){
        this->vector[i] = other.vector[i - temp];
    }
}

std::ostream &operator<<(std::ostream &os, const ShityVector &shityVector) {
    os << "size:" <<shityVector.size << "\n";
    for(int i = 0; i < shityVector.size; i++){
        os << shityVector.vector[i] << " ";
    }
    return os;
}

std::istream &operator>>(std::istream &is, const ShityVector &shityVector) {
    is >> shityVector.size;
    for(int i = 0; i < shityVector.size; i++){
        is >> shityVector.vector[i];
    }
    return is;
}

ShityVector::ShityVector(ShityVector &&other):vector(other.vector), size(other.size) {
    other.vector = nullptr;
}

ShityVector &ShityVector::operator=( ShityVector &&other) {
    if (&other == this) {
        return *this;
    }
    delete [] vector;
    this->vector = other.vector;
    other.vector = nullptr;
    return *this;
}


